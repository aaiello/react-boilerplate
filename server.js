/* eslint-disable no-console, no-process-exit, no-sync */
// const dev = process.env.NODE_ENV !== 'production'
// process.env.NODE_ENV = 'dev'
const dev = process.env.NODE_ENV !== 'production';
// process.env.NODE_ENV = 'dev'

if (dev) {
  require('dotenv').config({ path: '.env.development' });
}

const compression = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');

const logger = require('./logger');

const app = express();

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));
app.use(bodyParser.json({ extended: true, limit: '50mb' }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    parameterLimit: 100000,
    limit: '50mb',
  }),
);

const PORT = process.env.PORT || 3001;

app.use(morgan(dev ? 'dev' : 'common'));
app.use(compression({ threshold: 0 }));
app.use(cors());

app.use(cookieParser());

// Express only serves static assets in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'build')));
}

app.use('/api/v1/test', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const error = { message: 'Hola! Firma: Andy' };
  console.log(error);
  res.send(error);
});

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.use((req, res) => {
  res.render('404', { status: 404, url: req.url });
});

app.listen(PORT, error => {
  if (error) {
    logger.error(error);
    return process.exit(1);
  }

  logger.info(`Running in ${process.env.NODE_ENV} mode`);
  logger.info(`Find the server at: http://0.0.0.0:${PORT}/`);
});
